/**
 * @author Bruno Nogueira de Oliveira
 * @date 13/05/17.
 */
public class Cliente extends Pessoa {

    private int codigo;

    public Cliente(String nome, Data nascimento, int codigo) {
        super(nome, nascimento);
        this.codigo = codigo;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String imprimirDados() {
        final StringBuilder sb = new StringBuilder();
        sb.append("Código: " + getCodigo() + "\n");
        sb.append("Nome: " + getNome() + "\n");
        sb.append("Data de nascimento: " + getNascimento() + "\n");
        return sb.toString();
    }
}
