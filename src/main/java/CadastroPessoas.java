import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 13/05/17.
 */
public class CadastroPessoas {

    private List<Pessoa> pessoas;

    public CadastroPessoas() {
        pessoas = new ArrayList<Pessoa>();
    }

    public void cadastrar(Pessoa pessoa) {
        pessoas.add(pessoa);
    }

    public String imprimirCadastro() {
        final Iterator<Pessoa> iterator = pessoas.iterator();
        final StringBuilder sb = new StringBuilder();

        while (iterator.hasNext()) {
            Pessoa p = iterator.next();
            sb.append(p.imprimirDados());
            sb.append("\n");
        }

        return sb.toString();
    }
}
