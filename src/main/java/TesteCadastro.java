/**
 * @author Bruno Nogueira de Oliveira
 * @date 13/05/17.
 */
public class TesteCadastro {

    public static void main(String... args) {
        final CadastroPessoas cadastroPessoas = new CadastroPessoas();

        cadastroPessoas.cadastrar(new Cliente("João da Silva", new Data(1, 1, 1990), 1));
        cadastroPessoas.cadastrar(new Cliente("Maria Helena", new Data(15, 10, 1987), 2));
        cadastroPessoas.cadastrar(new Cliente("José Pereira", new Data(30, 4 , 1992), 3));
        cadastroPessoas.cadastrar(new Cliente("Joana D'Arc", new Data(6, 7, 1989), 4));

        cadastroPessoas.cadastrar(new Funcionario("Paulo Henrique", new Data(15, 2,1980), 1500f));
        cadastroPessoas.cadastrar(new Funcionario("Adriana Nogueira", new Data(7, 6,1992), 1200f));
        cadastroPessoas.cadastrar(new Funcionario("João Pereira", new Data(10, 12,1986), 1400f));

        cadastroPessoas.cadastrar(new Gerente("Lucas Cavaleiro", new Data(4, 5, 1978), 2500f, "Desenvolvimento"));
        cadastroPessoas.cadastrar(new Gerente("Fernanda Araujo", new Data(29, 1, 1988), 3000f, "Comercial"));

        System.out.println(cadastroPessoas.imprimirCadastro());

    }
}
