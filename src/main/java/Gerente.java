/**
 * @author Bruno Nogueira de Oliveira
 * @date 13/05/17.
 */
public class Gerente extends Funcionario {

    private String area;

    public Gerente(String nome, Data nascimento, float salario, String area) {
        super(nome, nascimento, salario);
        this.area = area;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    @Override
    public float calcularImposto() {
        return getSalario() * 0.05f;
    }

    @Override
    public String imprimirDados() {
        final StringBuilder sb = new StringBuilder();
        sb.append(super.imprimirDados());
        sb.append("Área: " + getArea() + "\n");
        return sb.toString();
    }
}
