/**
 * @author Bruno Nogueira de Oliveira
 * @date 13/05/17.
 */
public class Funcionario extends Pessoa implements PagadorImposto {

    private float salario;

    public Funcionario(String nome, Data nascimento, float salario) {
        super(nome, nascimento);
        this.salario = salario;
    }

    public float getSalario() {
        return salario;
    }

    public void setSalario(float salario) {
        this.salario = salario;
    }

    public float calcularImposto() {
        return getSalario() * 0.03f;
    }

    public String imprimirDados() {
        final StringBuilder sb = new StringBuilder();
        sb.append("Nome: " + getNome() + "\n");
        sb.append("Data de nascimento: " + getNascimento() + "\n");
        sb.append("Salario: " + getSalario() + "\n");
        sb.append("Imposto: " + calcularImposto() + "\n");
        return sb.toString();
    }


}
